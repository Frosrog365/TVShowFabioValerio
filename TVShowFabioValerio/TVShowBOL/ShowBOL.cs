﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVShowDAL;
using TVShowEntities;

namespace TVShowBOL
{
    public class ShowBOL
    {
        ShowDAL dal = new ShowDAL();
        public bool insertar(Series serie)
        {
            return dal.insertar(serie);
        }

        public List<Series> CargarSeries()
        {
            return dal.CargarSeries();
        }

        public List<Eepisodio.Class1> CargarEpisodios(Show show)
        {
            return dal.CargarEpisodios(show);
        }

        public bool EpisodioVisto(Eepisodio.Class1 episodio)
        {
            return dal.EpisodioVisto(episodio);
        }
    }
}
