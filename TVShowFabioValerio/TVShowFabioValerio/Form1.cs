﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TVShowBOL;
using TVShowEntities;

namespace TVShowFabioValerio
{
    public partial class Form1 : Form
    {
        List<Series> shows;
        List<Eepisodio.Class1> episodios;
        ShowBOL bol;
        EpisodioBOL eBOL;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string url = @"http://api.tvmaze.com/search/shows?q=a";
            string json = new WebClient().DownloadString(url);
            shows = JsonConvert.DeserializeObject<List<Series>>(json);



            //foreach (Eepisodio.Class1 item in episiodios)
            //{
            //    listBox1.Items.Add(item.name);
            //}
            foreach (Series item in shows)
            {
                //Console.WriteLine(item.show.name);
                comboBox1.Items.Add(item.show.name);

            }
            bol = new ShowBOL();
            buttonEPS.Enabled = false;
            button2.Enabled = false;
            button5.Enabled = false;
            button6.Enabled = false;

            


        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            Series show = cargarShow(comboBox1.SelectedItem.ToString());
            string url;
            string json;
            url = String.Format("http://api.tvmaze.com/shows/{0}/episodes", show.show.id);
            json = new WebClient().DownloadString(url);
            episodios = JsonConvert.DeserializeObject<List<Eepisodio.Class1>>(json);
            if (episodios.Count == 0)
            {
                MessageBox.Show("Esa serie no tiene episodios");
                buttonEPS.Enabled = false;
                button2.Enabled = false;
                button5.Enabled = false;
                button6.Enabled = false;
                listBox1.Items.Clear();

            }
            else
            {


                foreach (Eepisodio.Class1 item in episodios)
                {
                    listBox1.Items.Add(item.name);

                }
            }

        }

        /// <summary>
        /// 
        /// Carga los shows dependiendo del texto que escriba
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        private Series cargarShow(string nombre)
        {
            for (int i = 0; i < shows.Count; i++)
            {
                if (shows[i].show.name.Equals(nombre))
                {
                    return shows[i];
                }
            }
            return null;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (bol.insertar(cargarShow(comboBox1.SelectedItem.ToString())))
            {
                MessageBox.Show("Serie Agregada");
            }
            for (int i = 0; i < episodios.Count; i++)
            {
                eBOL = new EpisodioBOL();
                eBOL.insertar(episodios[i], cargarShow(comboBox1.SelectedItem.ToString()));
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                comboBox1.Items.Clear();
                string url = String.Format("http://api.tvmaze.com/search/shows?q={0}", textBox1.Text.Trim());
                string json = new WebClient().DownloadString(url);
                shows = JsonConvert.DeserializeObject<List<Series>>(json);
            }
            catch
            {
                MessageBox.Show(String.Format("No hay series que inicien con {0}", textBox1.Text));
                button2.Enabled = false;
                button5.Enabled = false;
                button6.Enabled = false;
                listBox1.Items.Clear();


            }



            //foreach (Eepisodio.Class1 item in episiodios)
            //{
            //    listBox1.Items.Add(item.name);
            //}
            foreach (Series item in shows)
            {
                //Console.WriteLine(item.show.name);
                comboBox1.Items.Add(item.show.name);

            }
            
            try
            {
                comboBox1.SelectedIndex = 0;
                buttonEPS.Enabled = true;
                button2.Enabled = true;
            }
            catch
            {
                MessageBox.Show("No hay series");
                buttonEPS.Enabled = false;
                button2.Enabled = false;
                button5.Enabled = false;
                button6.Enabled = false;
                listBox1.Items.Clear();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            button5.Enabled = true;
            List<Series> series = bol.CargarSeries();
            for (int i = 0; i < series.Count; i++)
            {
                string url = String.Format("http://api.tvmaze.com/search/shows?q={0}", series[i].show.name);
                string json = new WebClient().DownloadString(url);
                shows = JsonConvert.DeserializeObject<List<Series>>(json);
            }
            List<Show> shows2 = new List<Show>();
            foreach (var item in series)
            {
                shows2.Add(item.show);
            }

            dataGridView1.DataSource = shows2;


        }

        private void button5_Click(object sender, EventArgs e)
        {
            cargarEpisodios();
            button6.Enabled = true;
        }

        /// <summary>
        /// Carga los episodios en el datagrid dependiendo de la serie que seleccione
        /// </summary>
        public void cargarEpisodios()
        {
            Show show;
            int row = dataGridView1.SelectedRows.Count > 0 ? dataGridView1.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                show = dataGridView1.CurrentRow.DataBoundItem as Show;
            }
            else
            {
                show = null;
            }
            List<Eepisodio.Class1> episodios = bol.CargarEpisodios(show);

            dataGridView2.DataSource = episodios;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Eepisodio.Class1 episodio;
            int row = dataGridView2.SelectedRows.Count > 0 ? dataGridView2.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                episodio = dataGridView2.CurrentRow.DataBoundItem as Eepisodio.Class1;
            }
            else
            {
                episodio = null;
            }
            if (bol.EpisodioVisto(episodio))
            {
                MessageBox.Show("Episodio Visto!");
                cargarEpisodios();
            }
        }
    }
}
