﻿namespace TVShowFabioValerio
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.buttonEPS = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button4 = new System.Windows.Forms.Button();
            this.showBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.showBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.class1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button5 = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.urlDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seasonDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.airdateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.airtimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.airstampDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.runtimeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imageDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.summaryDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linksDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vistoDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.urlDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.languageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.runtimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.premieredDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.officialSiteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scheduleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ratingDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weightDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.networkDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.externalsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.summaryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.updatedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.linksDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.webChannelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button6 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.class1BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 38);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(156, 12);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(165, 225);
            this.listBox1.TabIndex = 1;
            // 
            // buttonEPS
            // 
            this.buttonEPS.Location = new System.Drawing.Point(13, 214);
            this.buttonEPS.Name = "buttonEPS";
            this.buttonEPS.Size = new System.Drawing.Size(120, 23);
            this.buttonEPS.TabIndex = 2;
            this.buttonEPS.Text = "Cargar Episodios";
            this.buttonEPS.UseVisualStyleBackColor = true;
            this.buttonEPS.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(13, 244);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(142, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Guardar en Favoritos";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(13, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 4;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(13, 185);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(120, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "Cargar Serie";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.urlDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.typeDataGridViewTextBoxColumn,
            this.languageDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn,
            this.runtimeDataGridViewTextBoxColumn,
            this.premieredDataGridViewTextBoxColumn,
            this.officialSiteDataGridViewTextBoxColumn,
            this.scheduleDataGridViewTextBoxColumn,
            this.ratingDataGridViewTextBoxColumn,
            this.weightDataGridViewTextBoxColumn,
            this.networkDataGridViewTextBoxColumn,
            this.externalsDataGridViewTextBoxColumn,
            this.imageDataGridViewTextBoxColumn,
            this.summaryDataGridViewTextBoxColumn,
            this.updatedDataGridViewTextBoxColumn,
            this.linksDataGridViewTextBoxColumn,
            this.webChannelDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.showBindingSource1;
            this.dataGridView1.Location = new System.Drawing.Point(394, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView1.RowHeadersWidth = 50;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(251, 225);
            this.dataGridView1.TabIndex = 6;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(394, 244);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(133, 23);
            this.button4.TabIndex = 7;
            this.button4.Text = "Ver Series Favoritas";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // showBindingSource
            // 
            this.showBindingSource.DataSource = typeof(TVShowEntities.Show);
            // 
            // showBindingSource1
            // 
            this.showBindingSource1.DataSource = typeof(TVShowEntities.Show);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.urlDataGridViewTextBoxColumn1,
            this.nameDataGridViewTextBoxColumn1,
            this.seasonDataGridViewTextBoxColumn,
            this.numberDataGridViewTextBoxColumn,
            this.airdateDataGridViewTextBoxColumn,
            this.airtimeDataGridViewTextBoxColumn,
            this.airstampDataGridViewTextBoxColumn,
            this.runtimeDataGridViewTextBoxColumn1,
            this.imageDataGridViewTextBoxColumn1,
            this.summaryDataGridViewTextBoxColumn1,
            this.linksDataGridViewTextBoxColumn1,
            this.vistoDataGridViewCheckBoxColumn});
            this.dataGridView2.DataSource = this.class1BindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(394, 273);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(223, 161);
            this.dataGridView2.TabIndex = 8;
            // 
            // class1BindingSource
            // 
            this.class1BindingSource.DataSource = typeof(TVShowEntities.Eepisodio.Class1);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(533, 244);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(133, 23);
            this.button5.TabIndex = 9;
            this.button5.Text = "Cargar Episodios";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "webChannel";
            this.dataGridViewTextBoxColumn1.HeaderText = "webChannel";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            this.idDataGridViewTextBoxColumn1.Visible = false;
            // 
            // urlDataGridViewTextBoxColumn1
            // 
            this.urlDataGridViewTextBoxColumn1.DataPropertyName = "url";
            this.urlDataGridViewTextBoxColumn1.HeaderText = "url";
            this.urlDataGridViewTextBoxColumn1.Name = "urlDataGridViewTextBoxColumn1";
            this.urlDataGridViewTextBoxColumn1.ReadOnly = true;
            this.urlDataGridViewTextBoxColumn1.Visible = false;
            // 
            // nameDataGridViewTextBoxColumn1
            // 
            this.nameDataGridViewTextBoxColumn1.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn1.HeaderText = "Nombre del Episodio";
            this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
            this.nameDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // seasonDataGridViewTextBoxColumn
            // 
            this.seasonDataGridViewTextBoxColumn.DataPropertyName = "season";
            this.seasonDataGridViewTextBoxColumn.HeaderText = "season";
            this.seasonDataGridViewTextBoxColumn.Name = "seasonDataGridViewTextBoxColumn";
            this.seasonDataGridViewTextBoxColumn.ReadOnly = true;
            this.seasonDataGridViewTextBoxColumn.Visible = false;
            // 
            // numberDataGridViewTextBoxColumn
            // 
            this.numberDataGridViewTextBoxColumn.DataPropertyName = "number";
            this.numberDataGridViewTextBoxColumn.HeaderText = "number";
            this.numberDataGridViewTextBoxColumn.Name = "numberDataGridViewTextBoxColumn";
            this.numberDataGridViewTextBoxColumn.ReadOnly = true;
            this.numberDataGridViewTextBoxColumn.Visible = false;
            // 
            // airdateDataGridViewTextBoxColumn
            // 
            this.airdateDataGridViewTextBoxColumn.DataPropertyName = "airdate";
            this.airdateDataGridViewTextBoxColumn.HeaderText = "airdate";
            this.airdateDataGridViewTextBoxColumn.Name = "airdateDataGridViewTextBoxColumn";
            this.airdateDataGridViewTextBoxColumn.ReadOnly = true;
            this.airdateDataGridViewTextBoxColumn.Visible = false;
            // 
            // airtimeDataGridViewTextBoxColumn
            // 
            this.airtimeDataGridViewTextBoxColumn.DataPropertyName = "airtime";
            this.airtimeDataGridViewTextBoxColumn.HeaderText = "airtime";
            this.airtimeDataGridViewTextBoxColumn.Name = "airtimeDataGridViewTextBoxColumn";
            this.airtimeDataGridViewTextBoxColumn.ReadOnly = true;
            this.airtimeDataGridViewTextBoxColumn.Visible = false;
            // 
            // airstampDataGridViewTextBoxColumn
            // 
            this.airstampDataGridViewTextBoxColumn.DataPropertyName = "airstamp";
            this.airstampDataGridViewTextBoxColumn.HeaderText = "airstamp";
            this.airstampDataGridViewTextBoxColumn.Name = "airstampDataGridViewTextBoxColumn";
            this.airstampDataGridViewTextBoxColumn.ReadOnly = true;
            this.airstampDataGridViewTextBoxColumn.Visible = false;
            // 
            // runtimeDataGridViewTextBoxColumn1
            // 
            this.runtimeDataGridViewTextBoxColumn1.DataPropertyName = "runtime";
            this.runtimeDataGridViewTextBoxColumn1.HeaderText = "runtime";
            this.runtimeDataGridViewTextBoxColumn1.Name = "runtimeDataGridViewTextBoxColumn1";
            this.runtimeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.runtimeDataGridViewTextBoxColumn1.Visible = false;
            // 
            // imageDataGridViewTextBoxColumn1
            // 
            this.imageDataGridViewTextBoxColumn1.DataPropertyName = "image";
            this.imageDataGridViewTextBoxColumn1.HeaderText = "image";
            this.imageDataGridViewTextBoxColumn1.Name = "imageDataGridViewTextBoxColumn1";
            this.imageDataGridViewTextBoxColumn1.ReadOnly = true;
            this.imageDataGridViewTextBoxColumn1.Visible = false;
            // 
            // summaryDataGridViewTextBoxColumn1
            // 
            this.summaryDataGridViewTextBoxColumn1.DataPropertyName = "summary";
            this.summaryDataGridViewTextBoxColumn1.HeaderText = "summary";
            this.summaryDataGridViewTextBoxColumn1.Name = "summaryDataGridViewTextBoxColumn1";
            this.summaryDataGridViewTextBoxColumn1.ReadOnly = true;
            this.summaryDataGridViewTextBoxColumn1.Visible = false;
            // 
            // linksDataGridViewTextBoxColumn1
            // 
            this.linksDataGridViewTextBoxColumn1.DataPropertyName = "_links";
            this.linksDataGridViewTextBoxColumn1.HeaderText = "_links";
            this.linksDataGridViewTextBoxColumn1.Name = "linksDataGridViewTextBoxColumn1";
            this.linksDataGridViewTextBoxColumn1.ReadOnly = true;
            this.linksDataGridViewTextBoxColumn1.Visible = false;
            // 
            // vistoDataGridViewCheckBoxColumn
            // 
            this.vistoDataGridViewCheckBoxColumn.DataPropertyName = "Visto";
            this.vistoDataGridViewCheckBoxColumn.HeaderText = "Visto";
            this.vistoDataGridViewCheckBoxColumn.Name = "vistoDataGridViewCheckBoxColumn";
            this.vistoDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id Serie";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // urlDataGridViewTextBoxColumn
            // 
            this.urlDataGridViewTextBoxColumn.DataPropertyName = "url";
            this.urlDataGridViewTextBoxColumn.HeaderText = "url";
            this.urlDataGridViewTextBoxColumn.Name = "urlDataGridViewTextBoxColumn";
            this.urlDataGridViewTextBoxColumn.ReadOnly = true;
            this.urlDataGridViewTextBoxColumn.Visible = false;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Nombre";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // typeDataGridViewTextBoxColumn
            // 
            this.typeDataGridViewTextBoxColumn.DataPropertyName = "type";
            this.typeDataGridViewTextBoxColumn.HeaderText = "type";
            this.typeDataGridViewTextBoxColumn.Name = "typeDataGridViewTextBoxColumn";
            this.typeDataGridViewTextBoxColumn.ReadOnly = true;
            this.typeDataGridViewTextBoxColumn.Visible = false;
            // 
            // languageDataGridViewTextBoxColumn
            // 
            this.languageDataGridViewTextBoxColumn.DataPropertyName = "language";
            this.languageDataGridViewTextBoxColumn.HeaderText = "language";
            this.languageDataGridViewTextBoxColumn.Name = "languageDataGridViewTextBoxColumn";
            this.languageDataGridViewTextBoxColumn.ReadOnly = true;
            this.languageDataGridViewTextBoxColumn.Visible = false;
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "status";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            this.statusDataGridViewTextBoxColumn.ReadOnly = true;
            this.statusDataGridViewTextBoxColumn.Visible = false;
            // 
            // runtimeDataGridViewTextBoxColumn
            // 
            this.runtimeDataGridViewTextBoxColumn.DataPropertyName = "runtime";
            this.runtimeDataGridViewTextBoxColumn.HeaderText = "runtime";
            this.runtimeDataGridViewTextBoxColumn.Name = "runtimeDataGridViewTextBoxColumn";
            this.runtimeDataGridViewTextBoxColumn.ReadOnly = true;
            this.runtimeDataGridViewTextBoxColumn.Visible = false;
            // 
            // premieredDataGridViewTextBoxColumn
            // 
            this.premieredDataGridViewTextBoxColumn.DataPropertyName = "premiered";
            this.premieredDataGridViewTextBoxColumn.HeaderText = "premiered";
            this.premieredDataGridViewTextBoxColumn.Name = "premieredDataGridViewTextBoxColumn";
            this.premieredDataGridViewTextBoxColumn.ReadOnly = true;
            this.premieredDataGridViewTextBoxColumn.Visible = false;
            // 
            // officialSiteDataGridViewTextBoxColumn
            // 
            this.officialSiteDataGridViewTextBoxColumn.DataPropertyName = "officialSite";
            this.officialSiteDataGridViewTextBoxColumn.HeaderText = "officialSite";
            this.officialSiteDataGridViewTextBoxColumn.Name = "officialSiteDataGridViewTextBoxColumn";
            this.officialSiteDataGridViewTextBoxColumn.ReadOnly = true;
            this.officialSiteDataGridViewTextBoxColumn.Visible = false;
            // 
            // scheduleDataGridViewTextBoxColumn
            // 
            this.scheduleDataGridViewTextBoxColumn.DataPropertyName = "schedule";
            this.scheduleDataGridViewTextBoxColumn.HeaderText = "schedule";
            this.scheduleDataGridViewTextBoxColumn.Name = "scheduleDataGridViewTextBoxColumn";
            this.scheduleDataGridViewTextBoxColumn.ReadOnly = true;
            this.scheduleDataGridViewTextBoxColumn.Visible = false;
            // 
            // ratingDataGridViewTextBoxColumn
            // 
            this.ratingDataGridViewTextBoxColumn.DataPropertyName = "rating";
            this.ratingDataGridViewTextBoxColumn.HeaderText = "rating";
            this.ratingDataGridViewTextBoxColumn.Name = "ratingDataGridViewTextBoxColumn";
            this.ratingDataGridViewTextBoxColumn.ReadOnly = true;
            this.ratingDataGridViewTextBoxColumn.Visible = false;
            // 
            // weightDataGridViewTextBoxColumn
            // 
            this.weightDataGridViewTextBoxColumn.DataPropertyName = "weight";
            this.weightDataGridViewTextBoxColumn.HeaderText = "weight";
            this.weightDataGridViewTextBoxColumn.Name = "weightDataGridViewTextBoxColumn";
            this.weightDataGridViewTextBoxColumn.ReadOnly = true;
            this.weightDataGridViewTextBoxColumn.Visible = false;
            // 
            // networkDataGridViewTextBoxColumn
            // 
            this.networkDataGridViewTextBoxColumn.DataPropertyName = "network";
            this.networkDataGridViewTextBoxColumn.HeaderText = "network";
            this.networkDataGridViewTextBoxColumn.Name = "networkDataGridViewTextBoxColumn";
            this.networkDataGridViewTextBoxColumn.ReadOnly = true;
            this.networkDataGridViewTextBoxColumn.Visible = false;
            // 
            // externalsDataGridViewTextBoxColumn
            // 
            this.externalsDataGridViewTextBoxColumn.DataPropertyName = "externals";
            this.externalsDataGridViewTextBoxColumn.HeaderText = "externals";
            this.externalsDataGridViewTextBoxColumn.Name = "externalsDataGridViewTextBoxColumn";
            this.externalsDataGridViewTextBoxColumn.ReadOnly = true;
            this.externalsDataGridViewTextBoxColumn.Visible = false;
            // 
            // imageDataGridViewTextBoxColumn
            // 
            this.imageDataGridViewTextBoxColumn.DataPropertyName = "image";
            this.imageDataGridViewTextBoxColumn.HeaderText = "image";
            this.imageDataGridViewTextBoxColumn.Name = "imageDataGridViewTextBoxColumn";
            this.imageDataGridViewTextBoxColumn.ReadOnly = true;
            this.imageDataGridViewTextBoxColumn.Visible = false;
            // 
            // summaryDataGridViewTextBoxColumn
            // 
            this.summaryDataGridViewTextBoxColumn.DataPropertyName = "summary";
            this.summaryDataGridViewTextBoxColumn.HeaderText = "summary";
            this.summaryDataGridViewTextBoxColumn.Name = "summaryDataGridViewTextBoxColumn";
            this.summaryDataGridViewTextBoxColumn.ReadOnly = true;
            this.summaryDataGridViewTextBoxColumn.Visible = false;
            // 
            // updatedDataGridViewTextBoxColumn
            // 
            this.updatedDataGridViewTextBoxColumn.DataPropertyName = "updated";
            this.updatedDataGridViewTextBoxColumn.HeaderText = "updated";
            this.updatedDataGridViewTextBoxColumn.Name = "updatedDataGridViewTextBoxColumn";
            this.updatedDataGridViewTextBoxColumn.ReadOnly = true;
            this.updatedDataGridViewTextBoxColumn.Visible = false;
            // 
            // linksDataGridViewTextBoxColumn
            // 
            this.linksDataGridViewTextBoxColumn.DataPropertyName = "_links";
            this.linksDataGridViewTextBoxColumn.HeaderText = "_links";
            this.linksDataGridViewTextBoxColumn.Name = "linksDataGridViewTextBoxColumn";
            this.linksDataGridViewTextBoxColumn.ReadOnly = true;
            this.linksDataGridViewTextBoxColumn.Visible = false;
            // 
            // webChannelDataGridViewTextBoxColumn
            // 
            this.webChannelDataGridViewTextBoxColumn.DataPropertyName = "webChannel";
            this.webChannelDataGridViewTextBoxColumn.HeaderText = "webChannel";
            this.webChannelDataGridViewTextBoxColumn.Name = "webChannelDataGridViewTextBoxColumn";
            this.webChannelDataGridViewTextBoxColumn.ReadOnly = true;
            this.webChannelDataGridViewTextBoxColumn.Visible = false;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(623, 411);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 10;
            this.button6.Text = "Visto";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.buttonEPS);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.comboBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.class1BindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button buttonEPS;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.BindingSource showBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource showBindingSource1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.BindingSource class1BindingSource;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn urlDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn languageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn runtimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn premieredDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn officialSiteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn scheduleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ratingDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn weightDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn networkDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn externalsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn imageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn summaryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn updatedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn linksDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn webChannelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn urlDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn seasonDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn airdateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn airtimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn airstampDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn runtimeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn imageDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn summaryDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn linksDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn vistoDataGridViewCheckBoxColumn;
        private System.Windows.Forms.Button button6;
    }
}

