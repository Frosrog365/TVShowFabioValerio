﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVShowEntities;

namespace TVShowDAL
{
    public class ShowDAL
    {
        /// <summary>
        /// Inserta las series en la base de datos
        /// </summary>
        /// <param name="serie"></param>
        /// <returns></returns>
        public bool insertar(Series serie)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"INSERT INTO serie(id_serie, nombre)values 
                                (@id,@nombre)";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", serie.show.id);
                cmd.Parameters.AddWithValue("@nombre", serie.show.name);

                return cmd.ExecuteNonQuery() > 0;
            }
        }

        /// <summary>
        /// Edita el bool en la base de datos de los episodios
        /// </summary>
        /// <param name="episodio"></param>
        /// <returns></returns>
        public bool EpisodioVisto(Eepisodio.Class1 episodio)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {

                con.Open();
                string sql = @"UPDATE episodio
	                            SET visto=1 where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", episodio.id);
                return cmd.ExecuteNonQuery() > 0;
            }
        }

        /// <summary>
        /// Carga los episodios de la base de datos
        /// </summary>
        /// <param name="show"></param>
        /// <returns></returns>
        public List<Eepisodio.Class1> CargarEpisodios(Show show)
        {
            List<Eepisodio.Class1> episodios = new List<Eepisodio.Class1>();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select * from episodio where id_serie = @id order by id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", show.id);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    episodios.Add(CargarEpidosio(reader));
                }

            }

            return episodios;
        }

        /// <summary>
        /// Carga el episodio por separado
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private Eepisodio.Class1 CargarEpidosio(NpgsqlDataReader reader)
        {
            Eepisodio.Class1 episodio = new Eepisodio.Class1
            {
                name = reader["Nombre"].ToString(),
                id = int.Parse(reader["id"].ToString()),
            };
            if (reader["visto"].ToString().Equals("0"))

            {
                episodio.visto = false;
            }
            else
            {
                episodio.visto = true;
            }
            return episodio;
        }

        /// <summary>
        /// Carga las series
        /// </summary>
        /// <returns></returns>
        public List<Series> CargarSeries()
        {
            List<Series> series = new List<Series>();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select * from serie";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    series.Add(CargarSerie(reader));
                }

            }

            return series;
        }

        /// <summary>
        /// Carga la serie por separado
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private Series CargarSerie(NpgsqlDataReader reader)
        {
            Series usu = new Series();
            usu.show = new Show();
            usu.show.name = reader["nombre"].ToString();
            usu.show.id = int.Parse(reader["id_serie"].ToString());

            return usu;
        }

        /// <summary>
        /// Inserta los episodios en la base de datos
        /// </summary>
        /// <param name="class1"></param>
        /// <param name="serie"></param>
        /// <returns></returns>
        public bool InsertarEpisodio(Eepisodio.Class1 class1, Series serie)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"INSERT INTO episodio(id_serie, nombre)values 
                                (@id,@nombre)";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@nombre", class1.name);
                cmd.Parameters.AddWithValue("@id", serie.show.id);

                return cmd.ExecuteNonQuery() > 0;
            }
        }
    }
}
